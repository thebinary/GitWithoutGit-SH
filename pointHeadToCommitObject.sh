#!/bin/bash

#Author	    :	thebinary <binary4bytes@gmail.com>
#Date	    :	Fri Dec 30 23:59:14 NPT 2016-12-30
#Purpose    :	Point head of current branch to given commit hashobject

echo "$1" > .git/$(cat .git/HEAD | awk -F': ' '{print $2}')

