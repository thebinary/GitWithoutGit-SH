#!/bin/bash

#Author	    :	thebinary <binary4bytes@gmail.com>
#Date	    :	Fri Dec 30 16:51:29 NPT 2016-12-30
#Purpose    :	Delete GIT Branch

rm -rvf .git/refs/heads/$1
