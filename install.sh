#!/bin/bash

#Author	    :	thebinary <binary4bytes@gmail.com>
#Date	    :	Sat Dec 31 02:02:11 NPT 2016-12-31
#Purpose    :	Install gitwogit_* links to ~/bin/

for file in $(ls *.sh | sed 's/.sh//g')
do
    ln -s $(pwd)/$file.sh ~/bin/gitwogit_$file
done


