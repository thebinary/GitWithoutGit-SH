#!/bin/bash

#Author	    :	thebinary <binary4bytes@gmail.com>
#Date	    :	Fri Dec 30 15:52:35 NPT 2016-12-30
#Purpose    :	Generate git-hash for given file


file="$1"
#tmpblob=$(mktemp /tmp/gitblob.tmp.XXXXX)

#echo "Generating blob file prepending git-blob magic number to the contents of file"
#(echo -n "blob "`wc -c $file  | awk '{print $1}'`; echo -en "\00"; cat $file) > $tmpblob

hash_file_name=$(openssl sha1 $1 | awk -F'= ' '{print $2}')
echo "BLOB HASH = $hash_file_name"

objectsdir=".git/objects"
hash_file_base=$(echo "$hash_file_name" | cut -c 1-2)
hash_remaining=$(echo "$hash_file_name" | cut -c 3-)

outfile="$objectsdir/$hash_file_base/$hash_remaining"
echo "BLOB OBJECT = $outfile"

#echo "Removing the temporary blob object"
#rm -f $tmpblob
