#!/bin/bash

#Author	    :	thebinary <binary4bytes@gmail.com>
#Date	    :	Fri Dec 30 16:51:29 NPT 2016-12-30
#Purpose    :	Create new branch

mkdir -p .git/refs/heads
cat .git/$(cat .git/HEAD | awk -F': ' '{print $2}') > .git/refs/heads/$1
