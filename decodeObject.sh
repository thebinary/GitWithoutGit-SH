#!/bin/bash

#Author	    :	thebinary <binary4bytes@gmail.com>
#Date	    :	Fri Dec 30 16:19:00 NPT 2016-12-30
#Purpose    :	Decode git-hashed object

python -c 'import sys; import zlib; content=open(sys.argv[1]).read(); sys.stdout.write(zlib.decompress(content));' "$1"
