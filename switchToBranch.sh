#!/bin/bash

#Author	    :	thebinary <binary4bytes@gmail.com>
#Date	    :	Sat Dec 31 01:21:37 NPT 2016-12-31
#Purpose    :	Switch current working branch

echo -en "ref: refs/heads/$1" > .git/HEAD
