#!/bin/bash

#Author	    :	thebinary <binary4bytes@gmail.com>
#Date	    :	Fri Dec 30 15:52:35 NPT 2016-12-30
#Purpose    :	Generate git-hash for given file



datetime=$(date +"%s %z")
message="$1"
tree="$2"
parent="$3"
author="$4"

#datetime="1483098055 +0545"
author='thebinary <binary4bytes@gmail.com>'
#message='first'
#tree="50de0b199f4bd81cd951f00e6425186cd9608fe5"

tmpcommitfile=$(mktemp /tmp/gitcommitfile.tmp.XXXXX)
tmpcommit=$(mktemp /tmp/gitcommit.tmp.XXXXX)

echo "INFO: Generating commit file"
echo "DEBUG: $tmpcommitfile"
(
echo -en "tree $tree\n"

cat <<EOF
parent $parent
author $author $datetime
committer $author $datetime

$message
EOF
) > $tmpcommitfile

echo "INFO: Getting commit file characters count"
count=$(wc -c $tmpcommitfile | awk '{print $1}')
echo "DEBUG: CHARACTERS COUNT = $count"

echo "INFO: Generating commit object file by prepending git-commit magic number to the contents of commit-file"
echo "DEBUG: $tmpcommit"
(
echo -en "commit $count\0"
cat $tmpcommitfile
) > $tmpcommit

hash_file_name=$(shasum $tmpcommit | awk '{print $1}')
echo "DEBUG: BLOB HASH = $hash_file_name"

objectsdir=".git/objects"
hash_file_base=$(echo "$hash_file_name" | cut -c 1-2)
hash_remaining=$(echo "$hash_file_name" | cut -c 3-)

outfile="$objectsdir/$hash_file_base/$hash_remaining"
echo "DEBUG: COMMIT OBJECT = $outfile"

mkdir -p "$objectsdir/$hash_file_base"
echo "Writing the blob file"
python -c 'import zlib; import sys; content=open(sys.argv[1]).read(); out=open(sys.argv[2], "w"); out.write(zlib.compress(content,1)); out.close()' $tmpcommit $outfile

echo "INFO: Removing the temporary files"
rm -f $tmpcommitfile
rm -f $tmpcommit
