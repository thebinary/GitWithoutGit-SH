#!/bin/bash

#Author	    :	thebinary <binary4bytes@gmail.com>
#Date	    :	Fri Dec 30 15:52:35 NPT 2016-12-30
#Purpose    :	Generate git-hash for given file



tmptreefile=$(mktemp /tmp/gittreefile.tmp.XXXXX)
tmptree=$(mktemp /tmp/gittree.tmp.XXXXX)

echo "INFO: Generating tree file"
echo "DEBUG: $tmptreefile"

(

for file in $@
do
    fileperm=$(stat -f"%p" $file)
    fileblobhash=$(./getBlobHash.sh $file | grep 'BLOB HASH' | awk -F'= ' '{print $2}')
    echo -en "$fileperm $file\0"
    python -c 'import sys; import struct; [sys.stdout.write(struct.pack("B", int(x,16))) for x in sys.argv[1:]]' $(echo -en "$fileblobhash" | sed 's/../& /g') 
done

) > $tmptreefile

echo "INFO: Getting tree file characters count"
count=$(wc -c $tmptreefile | awk '{print $1}')
echo "DEBUG: CHARACTERS COUNT = $count"

echo "INFO: Generating tree object file by prepending git-tree magic number to the contents of tree-file"
echo "DEBUG: $tmptree"
(
echo -en "tree $count\0"
cat $tmptreefile
) > $tmptree

hash_file_name=$(shasum $tmptree | awk '{print $1}')
echo "DEBUG: BLOB HASH = $hash_file_name"

objectsdir=".git/objects"
hash_file_base=$(echo "$hash_file_name" | cut -c 1-2)
hash_remaining=$(echo "$hash_file_name" | cut -c 3-)

outfile="$objectsdir/$hash_file_base/$hash_remaining"
echo "DEBUG: COMMIT OBJECT = $outfile"

mkdir -p "$objectsdir/$hash_file_base"
echo "Writing the tree object"
python -c 'import zlib; import sys; content=open(sys.argv[1]).read(); out=open(sys.argv[2], "w"); out.write(zlib.compress(content,1)); out.close()' $tmptree $outfile

echo "INFO: Removing the temporary files"
rm -f $tmptreefile
rm -f $tmptree
