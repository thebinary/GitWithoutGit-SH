#!/bin/bash

#Author	    :	thebinary <binary4bytes@gmail.com>
#Date	    :	Fri Dec 30 12:29:50 NPT 2016-12-30
#Purpose    :	Init Git repo

BASE=".git"
if [ "$1" == "--bare" ]
then
    BASE="."
fi

HEAD="$BASE/HEAD"
REFS="$BASE/refs"
OBJECTS="$BASE/objects"

if [ "$BASE" != "." ]
then
    mkdir $BASE
fi

echo 'ref: refs/heads/master' > $HEAD
mkdir $REFS
mkdir $OBJECTS
